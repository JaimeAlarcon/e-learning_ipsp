<?php

   //conexión a la base de datos

   require_once("../config/conexion.php");

   class Curso extends Conectar{

       

       public function get_filas_curso(){

         $conectar= parent::conexion();
           
             $sql="select * from cursos";
             
             $sql=$conectar->prepare($sql);

             $sql->execute();

             $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);

             return $sql->rowCount();
        
        }


      //método para seleccionar registros

   	   public function get_curso(){

   	   	  $conectar=parent::conexion();
   	   	  parent::set_names();

   	   	  $sql="select * from cursos";

   	   	  $sql=$conectar->prepare($sql);
   	   	  $sql->execute();

   	   	  return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
   	   }

   	    //método para insertar registros

        public function registrar_curso($nombre,$descripcion,$estado){


           $conectar= parent::conexion();
           parent::set_names();

           $sql="insert into cursos
           values(null,?,?,?,?);";

          
            $sql=$conectar->prepare($sql);

            $sql->bindValue(1, $_POST["nombre"]);
            $sql->bindValue(2, $_POST["descripcion"]);
            // $id_categoria= $_POST["id_categoria"];
            // $num_elementos=0;

            //   while($num_elementos<count($id_categoria)){

                  $sql->bindValue(3, $_POST["id_categoria"]);
                  
                  //recorremos los permisos con este contador
                  // $num_elementos=$num_elementos+1;
              // }     
            
            $sql->bindValue(4, $_POST["estado"]);

            
            $sql->execute();
      
           
            
        }

        
         //este metodo es para validar el id del proveedor(luego llamamos el metodo de editar_estado()) 
        //el id_proveedor se envia por ajax cuando se editar el boton cambiar estado y que se ejecuta el evento onclick y llama la funcion de javascript
        public function get_curso_por_id($id_curso){

          $conectar= parent::conexion();

          //$output = array();

          $sql="select * from cursos where id_curso=?";

                $sql=$conectar->prepare($sql);

                $sql->bindValue(1, $id_curso);
                $sql->execute();

                return $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);


        } 

        /*metodo que valida si hay registros activos*/
        public function get_curso_por_id_estado($id_curso,$estado){

         $conectar= parent::conexion();

         //declaramos que el estado esté activo, igual a 1

         $estado=1;

          
        $sql="select * from cursos where id_curso=? and estado=?";

              $sql=$conectar->prepare($sql);

              $sql->bindValue(1, $id_curso);
               $sql->bindValue(2, $estado);
              $sql->execute();

              return $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);


         }


         public function editar_curso($nombre,$descripcion,$estado){

        	$conectar=parent::conexion();
        	parent::set_names();

        	
         require_once("Cursos.php");

         $curso = new Curso();

         //verifica si la cedula tiene registro asociado a compras
         //$proveedor_compras=$proveedor->get_proveedor_por_cedula_compras($_POST["cedula_proveedor"]);

          //verifica si la cedula tiene registro asociado a detalle_compras
         //$proveedor_detalle_compras=$proveedor->get_proveedor_por_cedula_detalle_compras($_POST["cedula_proveedor"]);

           //si la cedula del proveedor NO tiene registros asociados en las tablas compras y detalle_compras entonces se puede editar el proveedor completo
        // if(is_array($proveedor_compras)==true and count($proveedor_compras)==0 and is_array($proveedor_detalle_compras)==true and count($proveedor_detalle_compras)==0){


        //       $sql="update proveedor set 

        //          cedula=?,
        //          razon_social=?,
        //          telefono=?,
        //          correo=?,
        //          direccion=?,
        //          estado=?,
        //          id_usuario=?
        //          where 
        //          cedula=?

        //       ";
                
        //        //echo $sql; exit();

        //           $sql=$conectar->prepare($sql);

        //           $sql->bindValue(1, $_POST["cedula"]);
        //           $sql->bindValue(2, $_POST["razon"]);
        //           $sql->bindValue(3, $_POST["telefono"]);
        //           $sql->bindValue(4, $_POST["email"]);
        //           $sql->bindValue(5, $_POST["direccion"]);
        //           $sql->bindValue(6, $_POST["estado"]);
        //           $sql->bindValue(7, $_POST["id_usuario"]);
        //           $sql->bindValue(8, $_POST["cedula_proveedor"]);
        //           $sql->execute();


        //     } else {

                  
        //   //si el proveedor tiene registros asociados en compras y detalle_compras entonces no se edita el la cedula del proveedor y la razon social

           $sql="update cursos set 
              
               nombre=?,
               descripcion=?,
               
               estado=?,
               where 
               id_curso=?
                  ";

                $sql=$conectar->prepare($sql);

                $sql->bindValue(1, $_POST["nombre"]);
                $sql->bindValue(2, $_POST["descripcion"]);
                // $sql->bindValue(3, $_POST["id_categoria"]);
                $sql->bindValue(3, $_POST["estado"]);
                $sql->bindValue(4, $_POST["id_curso"]);
                $sql->execute();

            // }

         }


         //método si el proveedor existe en la base de datos
        //valida si existe la cedula, proveedor o correo, si existe entonces se hace el registro del proveedor
        public function get_datos_curso($curso, $correo){

           $conectar=parent::conexion();

          $sql="select * from cursos where correo=?";

           //echo $sql; exit();

           $sql=$conectar->prepare($sql);

            $sql->bindValue(1, $curso);
            $sql->bindValue(2, $correo);
           
            $sql->execute();

           //print_r($email); exit();

           return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
        }


          //método para activar Y/0 desactivar el estado del proveedor

        public function editar_estado($id_curso,$estado){

        	 $conectar=parent::conexion();

        	 //si el estado es igual a 0 entonces el estado cambia a 1
        	 //el parametro est se envia por via ajax
        	 if($_POST["est"]=="0"){

        	   $estado=1;

        	 } else {

        	 	 $estado=0;
        	 }

        	 $sql="update cursos set 
              
              estado=?
              where 
              id_curso=?

        	 ";

        	 $sql=$conectar->prepare($sql);

        	 $sql->bindValue(1,$estado);
        	 $sql->bindValue(2,$id_curso);
        	 $sql->execute();
        }

       
         public function eliminar_curso($id_curso){

              $conectar=parent::conexion();

              $sql="delete from cursos where id_cuso=?";

              $sql=$conectar->prepare($sql);

              $sql->bindValue(1, $id_curso);
              $sql->execute();

              return $resultado=$sql->fetch(PDO::FETCH_ASSOC);
      }


        public function get_curso_por_id_categoria($id_categoria){

           $conectar= parent::conexion();

     
           $sql="select * from cursos where id_categoria=?";

            $sql=$conectar->prepare($sql);

            $sql->bindValue(1, $id_categoria);
            $sql->execute();

            return $resultado= $sql->fetchAll(PDO::FETCH_ASSOC);


      }


        //consulta si la cedula del proveedor tiene una compra asociada
    //    public function get_proveedor_por_cedula_compras($cedula_proveedor){

             
    //          $conectar=parent::conexion();
    //          parent::set_names();


    //       $sql="select p.cedula,c.cedula_proveedor
                 
    //        from proveedor p 
              
    //           INNER JOIN compras c ON p.cedula=c.cedula_proveedor


    //           where p.cedula=?

    //           ";

    //          $sql=$conectar->prepare($sql);
    //          $sql->bindValue(1,$cedula_proveedor);
    //          $sql->execute();

    //          return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);

    // }

      
      //consulta si la cedula del proveedor tiene un detalle_compra asociado
      // public function get_proveedor_por_cedula_detalle_compras($cedula_proveedor){

      //       $conectar=parent::conexion();
      //        parent::set_names();


      //      $sql="select p.cedula,d.cedula_proveedor
      //      from producto p 
              
      //         INNER JOIN detalle_compras c ON p.cedula=d.cedula_proveedor


      //         where p.cedula=?

      //         ";

      //        $sql=$conectar->prepare($sql);
      //        $sql->bindValue(1,$cedula_proveedor);
      //        $sql->execute();

      //        return $resultado=$sql->fetchAll(PDO::FETCH_ASSOC);
    

      //  }




   
}


?>