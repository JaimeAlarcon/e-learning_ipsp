
var tabla;
 
//Función que se ejecuta al inicio
 function init(){

	 listar();

	 //cuando se da click al boton submit entonces se ejecuta la funcion guardaryeditar(e);
   $("#curso_form").on("submit",function(e)
   {

	   guardaryeditar(e);	
   })

	//cambia el titulo de la ventana modal cuando se da click al boton
   $("#add_button").click(function(){

			//habilita los campos cuando se agrega un registro nuevo ya que cuando se editaba un registro asociado entonces aparecia deshabilitado los campos
		   
			$("#nombre").attr('disabled', false);
			$("#descripcion").attr('disabled', false);
					   
		   $(".modal-title").text("Agregar Curso");
	   
	 });


	  //Mostramos los permisos
	  /*en este caso NO se envia un id_usuario ya que se va agregar un 
	  usuario nuevo, solo se enviaría cuando se edita y ahí se enviaría el id_usuario
	  que se está editando*/
	  $.post("../ajax/curso.php?op=permisos&id_curso=",function(r){
		   $("#permisos").html(r);
	});

 }

 //funcion que limpia los campos del formulario

  function limpiar(){

 
   $('#nombre').val("");
   
   $('#descripcion').val("");
  
   $('#estado').val("");
   $('#id_curso').val("");
   //limpia los checkbox
   $('input:checkbox').removeAttr('checked');
  }

   //function listar 

   function listar(){

	   tabla=$('#curso_data').dataTable({

	   "aProcessing": true,//Activamos el procesamiento del datatables
	   "aServerSide": true,//Paginación y filtrado realizados por el servidor
	   dom: 'Bfrtip',//Definimos los elementos del control de tabla
	   buttons: [		          
				   'copyHtml5',
				   'excelHtml5',
				   'csvHtml5',
				   'pdf'
			   ],

	   "ajax":

		  {
				   url: '../ajax/curso.php?op=listar',
				   type : "get",
				   dataType : "json",						
				   error: function(e){
					   console.log(e.responseText);	
				   }
			   },

		"bDestroy": true,
	   "responsive": true,
	   "bInfo":true,
	   "iDisplayLength": 10,//Por cada 10 registros hace una paginación
	   "order": [[ 0, "desc" ]],//Ordenar (columna,orden)

	   "language": {

			   "sProcessing":     "Procesando...",
			
			   "sLengthMenu":     "Mostrar _MENU_ registros",
			
			   "sZeroRecords":    "No se encontraron resultados",
			
			   "sEmptyTable":     "Ningún dato disponible en esta tabla",
			
			   "sInfo":           "Mostrando un total de _TOTAL_ registros",
			
			   "sInfoEmpty":      "Mostrando un total de 0 registros",
			
			   "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			
			   "sInfoPostFix":    "",
			
			   "sSearch":         "Buscar:",
			
			   "sUrl":            "",
			
			   "sInfoThousands":  ",",
			
			   "sLoadingRecords": "Cargando...",
			
			   "oPaginate": {
			
				   "sFirst":    "Primero",
			
				   "sLast":     "Último",
			
				   "sNext":     "Siguiente",
			
				   "sPrevious": "Anterior"
			
			   },
			
			   "oAria": {
			
				   "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			
				   "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			
			   }

			  }//cerrando language




	   }).DataTable();
   }
   
	//Mostrar datos del usuario en la ventana modal del formulario 

	function mostrar(id_usuario){

	$.post("../ajax/curso.php?op=mostrar",{id_curso : id_curso}, function(data, status)
   
	   { 

		data = JSON.parse(data);

				//si existe la cedula_relacion entonces tiene relacion con otras tablas
			   if(data.cedula_relacion){

					   $('#cursoModal').modal('show');

					   
					   
					   

					   $('#nombre').val(data.nombre);
					   
					   //desactiva el campo
					   $("#nombre").attr('disabled', 'disabled');

					  
					   
					   $('#descripcion').val(data.descricion);
				   
					   $('#estado').val(data.estado);
					   $('.modal-title').text("Editar Curso");
					   $('#id_usuario').val(id_usuario);

				 } else{

					   $('#cursoModal').modal('show');
				   
					   $('#nombre').val(data.nombre);
					   $("#nombre").attr('disabled', false);
					
					   $('#descripcion').val(data.descricion);
			   
				   
					   $('#estado').val(data.estado);
					   $('.modal-title').text("Editar Curso");
					   $('#id_curso').val(id_curso);
					   

				  }		

	   });

		//muestra los checkbox en la ventana modal de usuarios
	   $.post("../ajax/curso.php?op=permisos&id_curso="+id_curso,function(r){
		   $("#permisos").html(r);
	   });

  }

   //la funcion guardaryeditar(e); se llama cuando se da click al boton submit  
	 
	 function guardaryeditar(e){

		 e.preventDefault(); //No se activará la acción predeterminada del evento
		 var formData = new FormData($("#curso_form")[0]);

		 
		   
			
			  $.ajax({

					  url: "../ajax/curso.php?op=guardaryeditar",
				   type: "POST",
				   data: formData,
				   contentType: false,
				   processData: false,

				   success: function(datos){

					   console.log(datos);

					   $('#curso_form')[0].reset();
					   $('#cursoModal').modal('hide');

					   $('#resultados_ajax').html(datos);
					   $('#curso_data').DataTable().ajax.reload();
			   
					   limpiar();

				   }
			  });

		


	 }  
	  
	  //EDITAR ESTADO DEL USUARIO
	  //importante:id_usuario, est se envia por post via ajax
	  function cambiarEstado(id_usuario,est){
		   
	   bootbox.confirm("¿Está Seguro de cambiar de estado?", function(result){
	   if(result)
	   {
		  
		  $.ajax({
			   url:"../ajax/curso.php?op=activarydesactivar",
				method:"POST",
			   
			   //toma el valor del id y del estado
			   data:{id_curso:id_curso, est:est},
			   
			   success: function(data){
				
				 $('#curso_data').DataTable().ajax.reload();
			   
			   }

		   });

	  }

		});//bootbox

	  } 


	   //ELIMINAR USUARIO

	function eliminar(id_usuario){

  
	   bootbox.confirm("¿Está Seguro de eliminar el curso?", function(result){
	   if(result)
	   {

			   $.ajax({
				   url:"../ajax/curso.php?op=eliminar_curso",
				   method:"POST",
				   data:{id_curso:id_curso},

				   success:function(data)
				   {
					   //alert(data);
					   $("#resultados_ajax").html(data);
					   $("#curso_data").DataTable().ajax.reload();
				   }
			   });

			 }

		});//bootbox

  }



 init();






