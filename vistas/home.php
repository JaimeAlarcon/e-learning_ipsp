
<?php

require_once("../config/conexion.php");


?>


<?php require_once("header.php");?>

  
<div class="content-wrapper">


<div class="content-wrapper" style=" background: white;">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="padding: 30px;">
        <h1>
          ¡Hola KATHERINE!
          <small></small>
        </h1>
        
                <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel" data-type="multi">
                    <div class="carousel-inner">
                       
                         <!--Encabezado-->
            <div class="container" style="padding: 30px 30px;">
              
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <!-- Indicators -->
                          <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                          </ol>

                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                          <div class="item active">
                              <img src="../public/images/prueba6.jpg" alt="Los Angeles" style="width:100%;height:100%">
                          </div>

                          <div class="item">
                              <img src="../public/images/prueba4.jpg" alt="Chicago" style="width:100%;height:100%">
                          </div>
                      
                          <div class="item">
                              <img src="../public/images/foto1.png" alt="New york" style="width:100%;height:100%">
                          </div>
                      </div>

                      <!-- Left and right controls -->
                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                      <span class="sr-only">Previous</span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                      <span class="sr-only">Next</span>
                      </a>
              </div>
          </div>
    <!--Carousel FIN DEL HEADER-->
                </div>
                       
         </div>
         
        
          <div>
         

      <!-- ********************************************************************************************* -->
      <section>
         <!-- CAROUSEL DE VISTAS MIS CURSOS-->
        
         <div class="row">
               
                    <div class="col-md-6 col-sm-6 col-xs-12">
        
                        <h3>Cursos</h3>
                         <div style= "padding: 15px;"></div>
                    </div>
                    
                </div>
                <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel" data-type="multi">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso1.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Desarrollo Web con HTML5 , CSS3 , JS</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso3.PNG" class="img-responsive" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Java Enterprise Edition</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info">Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso4.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Curso Ofimática - Excel</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso5.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Power Point - Principiantes</h5>
                                                    <h5 class="detail-price">Enrique Gilbert</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso1.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Desarrollo Web con HTML5 , CSS3 , JS</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso3.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Java Enterprise Edition</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalle</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso4.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Curso Ofimática - Excel</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso5.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Power Point - Principiantes</h5>
                                                    <h5 class="detail-price">Enrique Gilbert</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                       
         </div>
         <div class="row">
         <div style= "padding: 15px;"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                       

                       
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                        <div class="controls pull-right">
                            <a class="left fa fa-chevron-left btn btn-info " href="#carousel-example1" data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-info" href="#carousel-example1" data-slide="next"></a>
                        </div>
                    </div>
                </div>
                <hr>
        <!-- CAROUSEL DE VISTAS CURSOS SUGERENCIAS-->
        <div class="row">
                       
                    <div class="col-md-6 col-sm-6 col-xs-12">
        
                        <h3>Sugerencias</h3>
                         <div style= "padding: 10px;"></div>
                    </div>
                    
                </div>
                <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel" data-type="multi">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso6.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Máster en PHP , SQL , POO</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso7.PNG" class="img-responsive" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Java Enterprise Edition</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info">Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso4.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Curso Ofimática - Excel</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso5.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Power Point - Principiantes</h5>
                                                    <h5 class="detail-price">Enrique Gilbert</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso1.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Desarrollo Web con HTML5 , CSS3 , JS</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso3.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Java Enterprise Edition</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalle</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso4.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Curso Ofimática - Excel</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso5.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Power Point - Principiantes</h5>
                                                    <h5 class="detail-price">Enrique Gilbert</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                       
         </div>
         <div class="row">
         <div style= "padding: 15px;"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                       

                       
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                        <div class="controls pull-right">
                            <a class="left fa fa-chevron-left btn btn-info " href="#carousel-example1" data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-info" href="#carousel-example1" data-slide="next"></a>
                        </div>
                    </div>
                </div>
                <hr>
        <!-- COROUSEL DE VISTAS SE CURSOS MAS DESTACADOS -->
         
         <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
        
                        <h3>Videos Destacados</h3>
                         <div style= "padding: 10px;"></div>
                    </div>
                    
                </div>
                <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel" data-type="multi">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso1.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Desarrollo Web con HTML5 , CSS3 , JS</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso3.PNG" class="img-responsive" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Java Enterprise Edition</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info">Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso4.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Curso Ofimática - Excel</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso5.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Power Point - Principiantes</h5>
                                                    <h5 class="detail-price">Enrique Gilbert</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso1.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Desarrollo Web con HTML5 , CSS3 , JS</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso3.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Java Enterprise Edition</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalle</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso4.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Curso Ofimática - Excel</h5>
                                                    <h5 class="detail-price">Víctor Robles</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="slider-item">
                                        <div class="slider-image">
                                            <img src="../public/images/curso5.PNG" class="img-responsive" alt="a" />
                                        </div>
                                        <div class="slider-main-detail">
                                            <div class="slider-detail">
                                                <div class="product-detail">
                                                    <h5>Aprende Power Point - Principiantes</h5>
                                                    <h5 class="detail-price">Enrique Gilbert</h5>
                                                </div>
                                            </div>
                                            <div class="cart-section">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-6 review">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-6">
                                                        <a href="#" class="AddCart btn btn-info"> Ver detalles</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                       
         </div>
         <div class="row">
         <div style= "padding: 15px;"></div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                       

                       
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                        <div class="controls pull-right">
                            <a class="left fa fa-chevron-left btn btn-info " href="#carousel-example1" data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-info" href="#carousel-example1" data-slide="next"></a>
                        </div>
                    </div>
                </div>
                 
        <br/><br/><br/>
        </section>
        


      </section>

      
    </div>
    <!-- /.container -->
  </div>



</div>


<?php require_once("footer.php");?>


