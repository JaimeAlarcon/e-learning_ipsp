<?php

 require_once("config/conexion.php");

     

     if(isset($_POST["enviar"]) and $_POST["enviar"]=="si"){

       require_once("modelos/Usuarios.php");

       $usuario = new Usuarios();

       $usuario->login();

   }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>e-learning</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="public/bower_components/bootstrap/dist/css/bootstrap.min.css">
 
  <!-- Theme style -->
  <link rel="stylesheet" href="public/dist/css/AdminLTE.min.css">

<!--===============================================================================================-->	
<link rel="icon" type="image/png" href="public/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="public/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="public/css/util.css">
	<link rel="stylesheet" type="text/css" href="public/css/main.css">
<!--===============================================================================================-->

</head>
<body >
<!--body-->
<div class="limiter">
		<div class="container-login100" style="background-image: url('public/images/img-01.jpg');">
    <div class="wrap-login100 p-t-190 p-b-30">

    <div class="login100-form-avatar">
						<img src="public/images/logo.png" alt="AVATAR">
					</div>
        <span class="login100-form-title p-t-20 p-b-45">
						Iniciar Sesión
				</span>
    
           
            <?php


            if(isset($_GET["m"])) {
               
           switch($_GET["m"]){


               case "1";
               ?>

               <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-ban"></i> El correo y/o password es incorrecto o no tienes permiso!</h4>
                     
                </div>

                <?php
                break;


                case "2";
                ?>
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-ban"></i> Los campos estan vacios</h4>
                     
                </div>
                <?php
                break;



             }

         }


            ?>

     
<!-- FIN MENSAJES DE ALERTA-->


    <form action="" method="post" class="login100-form valisdate-form">

          
    
              <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
                <input class="input100" type="text" name="correo" id="correo" placeholder="Username">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                  <i class="fa fa-user"></i>
                </span>
              </div>
     
      
     
              <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
                <input class="input100" type="password" name="password" id="password" placeholder="Password">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                  <i class="fa fa-lock"></i>
                </span>
              </div>
     

       <div class="form-group">
        <input type="hidden" name="enviar" class="form-control" value="si">
       
      </div>
      
        
          <div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn">
							Login
						</button>
          </div>
          
          <div class="text-center w-full p-t-25 p-b-230">
						<a href="#" class="txt1">
							Olvidaste la contraseña?
						</a>
					</div>

    </form>

   

    <!--<a href="#">I forgot my password</a><br>-->
  

<!-- jQuery 3 -->
<script src="public/bower_components/jquery/dist/jquery.min.js"></script>

<!-- iCheck -->
<script src="public/plugins/iCheck/icheck.min.js"></script>

<!--===============================================================================================-->	
<script src="public/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="public/vendor/bootstrap/js/popper.js"></script>
	<script src="public/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="public/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="public/js/main.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

